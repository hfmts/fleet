from frappe import _

def get_data():
	return [
		{
			"module_name": "Fleet",
			"type": "module",
			"label": _("Fleet")
		}
	]
